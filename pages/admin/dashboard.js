import { useState } from 'react'

//Components
import Navbar from '@components/admin/layout/Navbar'

// Icons
import { HiMenu } from 'react-icons/hi'
import { VscGraphLine } from 'react-icons/vsc'
import { FaRegUser } from 'react-icons/fa'
import { MdOutlineShoppingBag } from 'react-icons/md'

function Dashboard() {
  const [open, setOpen] = useState(false)

  return (
    <div className='flex'>
      <Navbar open={open} setOpen={setOpen} />
      <div
        className={`p-4 w-full md:ml-20 transition-all ${open && 'md:ml-80'}`}
      >
        {/* Header */}
        <div className='mb-8 w-full flex justify-between items-center'>
          <h1 className='text-4xl'>Dashboard</h1>
          <HiMenu
            size={50}
            className='md:hidden'
            onClick={() => setOpen(true)}
          />
        </div>
        {/* Info Cards */}
        <div className='grid grid-cols-3 gap-4 w-full'>
          <div className='flex flex-col justify-center md:flex-row md:justify-between items-center border shadow-md bg-slate-500 text-white rounded-lg px-4 py-6 text-3xl md:text-4xl lg:text-5xl'>
            <VscGraphLine className='hidden sm:block' />
            <div>
              <h3 className='text-base lg:text-lg font-medium'>Ganancias</h3>
              <p className='text-sm text-center lg:text-base md:ml-auto'>
                $12,000
              </p>
            </div>
          </div>
          <div className='flex flex-col justify-center md:flex-row md:justify-between items-center border shadow-md bg-slate-500 text-white rounded-lg px-4 py-6 text-3xl md:text-4xl lg:text-5xl'>
            <MdOutlineShoppingBag className='hidden sm:block' />
            <div>
              <h3 className='text-base lg:text-lg font-medium'>Pedidos</h3>
              <p className='text-sm text-center lg:text-base md:ml-auto'>110</p>
            </div>
          </div>
          <div className='flex flex-col justify-center md:flex-row md:justify-between items-center border shadow-md bg-slate-500 text-white rounded-lg px-4 py-6 text-3xl md:text-4xl lg:text-5xl'>
            <FaRegUser className='hidden sm:block' />
            <div>
              <h3 className='text-base lg:text-lg font-medium'>Usuarios</h3>
              <p className='text-sm text-center lg:text-base md:ml-auto'>20</p>
            </div>
          </div>
        </div>

        {/* Graphs */}
        <div className='grid lg:grid-cols-2 w-full gap-6 my-6 h-80'>
          <div className='w-full bg-slate-500 rounded-lg h-full'>p</div>
          <div className='w-full bg-slate-500 rounded-lg h-full'>p</div>
        </div>

        {/* Recent Orders */}
        <div className='w-full gap-6 my-6 h-80'>
          <div className='w-full bg-slate-500 rounded-lg h-full'>p</div>
        </div>
      </div>
    </div>
  )
}

export default Dashboard
