import { useState } from 'react'
import Link from 'next/link'
import axios from 'axios'
import { useRouter } from 'next/router'
import { useUIState } from '@hooks/useContextHook'

//Components
import DoughnutChart from '@components/charts/DoughnutChart'
import PieChart from '@components/charts/PieChart'
import Navbar from '@components/admin/layout/Navbar'

// Icons
import { HiMenu } from 'react-icons/hi'
import OrdersTable from '@components/table/OrdersTable'

export const getServerSideProps = async (ctx) => {
  const { data } = await axios.get(
    `http://localhost:3000/api/orders?state=${ctx.query.state}`
  )
  return {
    props: {
      orders: data,
    },
  }
}

function Pedidos({ orders }) {
  const router = useRouter()
  const [open, setOpen] = useState(false)
  const [selected, setSelected] = useState([])
  const { user } = useUIState()

  const handleChangeSelected = (e, id) => {
    if (e.target.checked) {
      setSelected((prev) => [...prev, id])
    } else {
      const newItems = selected.filter((i) => i !== id)
      setSelected(newItems)
    }
  }

  const handleSendOrder = async () => {
    let config = {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    }
    try {
      await axios.put('/api/orders/send', { orders: selected }, config)
      router.reload()
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <div className='flex'>
      <Navbar open={open} setOpen={setOpen} />
      <div
        className={`p-4 w-full md:ml-20 transition-all ${open && 'md:ml-80'}`}
      >
        {/* Header */}
        <div className='mb-8 w-full flex justify-between items-center'>
          <h1 className='text-4xl'>Pedidos</h1>
          <HiMenu
            size={50}
            className='md:hidden'
            onClick={() => setOpen(true)}
          />
        </div>

        {/* Info */}
        <div
          className='flex pl-2 pr-2 lg:pl-0 w-full gap-6 my-6 h-96 overflow-y-scroll border-2 not-scroll rounded-lg'
          style={{ height: '500px' }}
        >
          <div className='hidden lg:block w-1/3 rounded-lg h-full border-r'>
            <ul className='w-full p-4'>
              <li>
                <Link href='/admin/pedidos?state=pendiente'>
                  <a className='pedidos_links'>Pendientes</a>
                </Link>
              </li>

              <li>
                <Link href='/admin/pedidos?state=enviado'>
                  <a className='pedidos_links'>Enviados</a>
                </Link>
              </li>

              <li>
                <Link href='/admin/pedidos'>
                  <a className='pedidos_links'>Hoy</a>
                </Link>
              </li>

              <li>
                <Link href='/admin/pedidos?state=pendiente'>
                  <a className='pedidos_links'>Ayer</a>
                </Link>
              </li>
            </ul>
          </div>
          <div className='w-full rounded-lg h-full'>
            <OrdersTable
              orders={orders}
              handleChangeSelected={handleChangeSelected}
              selected={selected}
              handleSendOrder={handleSendOrder}
            />
          </div>
        </div>

        {/* Graphs */}
        <div className='grid lg:grid-cols-2 w-full gap-6 my-6 h-80'>
          <div className='w-full border-2 rounded-lg h-full'>
            <h1 className='text-3xl my-3 border-b-2 pl-2 pb-2'>Provincias</h1>
            <DoughnutChart />
          </div>
          <div className='w-full border-2 rounded-lg h-full'>
            <h1 className='text-3xl my-3 border-b-2 pl-2 pb-2'>
              Metodos de pago
            </h1>
            <PieChart />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Pedidos
