import nc from 'next-connect'
import { Category } from '@models/products'
import response from '@utils/response'
import { connect, disconnect } from '@utils/db'

const handler = nc()

handler.get(async (req, res) => {
  try {
    await connect()
    const categories = await Category.find({})
    await disconnect()
    response(res, 200, categories)
  } catch (err) {
    response(res, 500, err.message)
  }
})

handler.post(async (req, res) => {
  const { title } = req.body
  try {
    await connect()
    const exist = await Category.findOne({ title })
    if (exist) {
      response(res, 400, { error: 'Title already exist!' })
    } else {
      const category = await Category.create({ title })
      response(res, 200, category)
    }
  } catch (err) {
    response(res, 500, err.message)
  }
})

export default handler
