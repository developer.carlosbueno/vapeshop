import nc from 'next-connect'
import { Category } from '@models/products'
import response from '@utils/response'
import { connect, disconnect } from '@utils/db'

const handler = nc()

handler.get(async (req, res) => {
  try {
    await connect()
    const category = await Category.findById(req.query.id)
    await disconnect()
    response(res, 200, category)
  } catch (err) {
    response(res, 500, err.message)
  }
})

handler.put(async (req, res) => {
  const { title } = req.body
  try {
    await connect()
    const category = await Category.findById(req.query.id)
    category.title = title
    category.save()
    await disconnect()
    response(res, 200, category)
  } catch (err) {
    response(res, 500, err.message)
  }
})

handler.delete(async (req, res) => {
  try {
    await connect()
    await Category.findByIdAndDelete(req.query.id)
    await disconnect()
    response(res, 204, 'deleted')
  } catch (err) {
    response(res, 500, err.message)
  }
})

export default handler
