import nc from 'next-connect'
import User from '@models/users'
import response from '@utils/response'
import { connect, disconnect } from '@utils/db'

const handler = nc()

handler.get(async (req, res) => {
  try {
    await connect()
    const users = await User.find({})
    await disconnect()
    response(res, 200, users)
  } catch (err) {
    response(res, 500, err.message)
  }
})

export default handler
