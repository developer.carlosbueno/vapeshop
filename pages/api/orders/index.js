import nc from 'next-connect'
import Order from '@models/orders'
import response from '@utils/response'
import { protect, admin } from '@middlewares/authMiddleware'
import { connect, disconnect } from '@utils/db'

const handler = nc()

handler.get(async (req, res) => {
  const { state } = req.query
  let orders
  try {
    await connect()
    if (state.includes('undefined')) {
      orders = await Order.find({})
    } else {
      console.log('ok')
      orders = await Order.find({ state })
    }
    await disconnect()
    response(res, 200, orders)
  } catch (err) {
    response(res, 500, err.message)
  }
})

handler.post(async (req, res) => {
  const { orderItems, shipping, contact, totalPrice, paymentMethod } = req.body

  try {
    await connect()
    const order = await Order.create({
      orderItems,
      shipping,
      contact,
      totalPrice,
      paymentMethod,
      state: 'pendiente',
    })
    await disconnect()
    response(res, 200, order)
  } catch (err) {
    response(res, 500, err.message)
  }
})

export default handler
