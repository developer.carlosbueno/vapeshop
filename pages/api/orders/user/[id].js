import nc from 'next-connect'
import Order from '@models/orders'
import { protect } from '@middlewares/authMiddleware'
import { connect, disconnect } from '@utils/db'
import response from '@utils/response'

const handler = nc()

handler.use(protect).get(async (req, res) => {
  try {
    await connect()
    const orders = await Order.find({ user: req.user._id })
    await disconnect()
    response(res, 200, orders)
  } catch (err) {
    response(res, 500, err.message)
  }
})

export default handler
