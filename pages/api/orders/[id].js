import nc from 'next-connect'
import { Order } from '@models/index'
import { protect } from '@middlewares/authMiddleware'
import { connect, disconnect } from '@utils/db'
import response from '@utils/response'

const handler = nc()

handler.use(protect).get(async (req, res) => {
  try {
    await connect()
    const order = await Order.findById(req.query.id).populate({
      path: 'orderItems.product',
      select: 'title price',
    })
    await disconnect()
    response(res, 200, order)
  } catch (err) {
    response(res, 500, err.message)
  }
})

export default handler
