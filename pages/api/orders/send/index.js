import nc from 'next-connect'
import { Order } from '@models/index'
import { protect } from '@middlewares/authMiddleware'
import { connect, disconnect } from '@utils/db'
import response from '@utils/response'

const handler = nc()

handler.use(protect).put(async (req, res) => {
  const { orders } = req.body
  try {
    await connect()
    orders.forEach(async (order) => {
      const current = await Order.findById(order)
      current.state = 'enviado'
      await current.save()
    })
    await disconnect()
    response(res, 200, 'enviados')
  } catch (err) {
    response(res, 500, err.message)
  }
})

export default handler
