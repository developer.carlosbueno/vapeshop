import nc from 'next-connect'
import passport from 'passport'
import { connect } from '@utils/db'
import '@lib/passport'

const handler = nc()

handler.get(async (req, res, next) => {
  await connect()
  passport.authenticate('google', {
    scope: ['profile', 'email'],
    session: false,
  })(req, res, next)
})

export default handler
