import nc from 'next-connect'
import { setCookie } from 'cookies-next'
import passport from 'passport'
import { connect } from '@utils/db'
import '@lib/passport'

const handler = nc()

handler.get(async (req, res, next) => {
  await connect()
  passport.authenticate('google', (err, user, info) => {
    if (err || !user) res.redirect('http://localhost:3000/?a=auth_fail')
    setCookie(
      'userInfo',
      JSON.stringify({
        id: user._id,
        email: user.email,
        fullname: user.fullname,
        token: user.token,
      }),
      { req, res }
    )
    res.redirect('http://localhost:3000/')
  })(req, res, next)
})

export default handler
