import nc from 'next-connect'
import Product, { Brand, Category } from '@models/products'
import response from '@utils/response'
import { connect, disconnect } from '@utils/db'

const handler = nc()

handler.get(async (req, res) => {
  const { category, brand } = req.query
  let products
  try {
    await connect()
    const categorydb = await Category.findOne({ title: category })
    const branddb = await Brand.findOne({ title: brand })
    if (categorydb && branddb) {
      products = await Product.find({
        category: categorydb._id,
        brand: branddb._id,
      })
    } else if (categorydb) {
      products = await Product.find({ category: categorydb._id })
    } else if (branddb) {
      products = await Product.find({ brand: branddb })
    } else {
      products = await Product.find({})
    }
    await disconnect()
    response(res, 200, products)
  } catch (err) {
    response(res, 500, err.message)
  }
})

handler.post(async (req, res) => {
  const { title, price, description, stock, image, category, brand } = req.body

  try {
    await connect()
    const categorydb = await Category.findOne({ title: category })
    const branddb = await Brand.findOne({ title: brand })
    if (!categorydb._id) {
      response(res, 400, { error: 'That category dosent exist!' })
    } else {
      const product = await Product.create({
        title,
        price,
        description,
        stock,
        image,
        category: categorydb._id,
        brand: branddb._id,
      })
      await disconnect()
      response(res, 201, product)
    }
  } catch (err) {
    response(res, 500, err.message)
  }
})

export default handler
