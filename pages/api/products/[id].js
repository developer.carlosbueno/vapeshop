import nc from 'next-connect'
import Product from '@models/products'
import { connect, disconnect } from '@utils/db'
import response from '@utils/response'

const handler = nc()

// Get one product
handler.get(async (req, res) => {
  try {
    await connect()
    const product = await Product.findById(req.query.id)
    await disconnect()
    response(res, 200, product)
  } catch (err) {
    response(res, 500, err.message)
  }
})

// Update one product
handler.put(async (req, res) => {
  const { title, price, description, stock, image } = req.body

  try {
    await connect()
    const product = await Product.findById(req.query.id)
    product.title = title
    product.price = price
    product.description = description
    product.stock = stock
    product.image = image
    await product.save()
    await disconnect()
    response(res, 200, product)
  } catch (err) {
    response(res, 500, err.message)
  }
})

// Delete one product
handler.delete(async (req, res) => {
  try {
    await connect()
    await Product.findByIdAndDelete(req.query.id)
    await disconnect()
    response(res, 204, 'deleted')
  } catch (err) {
    response(res, 500, err.message)
  }
})

export default handler
