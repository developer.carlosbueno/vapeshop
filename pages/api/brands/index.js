import nc from 'next-connect'
import { Brand } from '@models/products'
import response from '@utils/response'
import { connect, disconnect } from '@utils/db'

const handler = nc()

handler.get(async (req, res) => {
  try {
    await connect()
    const brands = await Brand.find({})
    await disconnect()
    response(res, 200, brands)
  } catch (err) {
    response(res, 500, err.message)
  }
})

handler.post(async (req, res) => {
  const { title } = req.body
  try {
    await connect()
    const exist = await Brand.findOne({ title })
    if (exist) {
      response(res, 400, { error: 'Title already exist!' })
    } else {
      const brand = await Brand.create({ title })
      await disconnect()
      response(res, 200, brand)
    }
  } catch (err) {
    response(res, 500, err.message)
  }
})

export default handler
