import nc from 'next-connect'
import Product from '@models/products'
import Order from '@models/orders'
import { connect, disconnect } from '@utils/db'
import { Category, Brand } from '@models/products'
import { products, categories, brands } from '@utils/data'

const handler = nc()

handler.get(async (req, res) => {
  await connect()
  // await Product.deleteMany({})
  // await Category.deleteMany({})
  // await Brand.deleteMany({})
  // await Product.insertMany(products)
  // await Category.insertMany(categories)
  // await Brand.insertMany(brands)

  await Order.deleteMany({})
  await disconnect()
  res.status(200).redirect('/')
})

export default handler
