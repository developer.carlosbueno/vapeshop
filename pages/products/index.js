//Components
import Layout from '@components/layout/Layout'
import ProductsContainer from '@components/products/ProductsContainer'
import axios from 'axios'

export const getServerSideProps = async (ctx) => {
  const { category, brand } = ctx.query
  const { data } = await axios.get(
    `http://localhost:3000/api/products?category=${
      category && category
    }&brand=${brand && brand}`
  )
  return {
    props: {
      products: data,
    },
  }
}

export default function ProductsPage({ products }) {
  return (
    <Layout>
      <ProductsContainer products={products} />
    </Layout>
  )
}
