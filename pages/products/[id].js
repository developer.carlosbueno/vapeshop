// Components
import Layout from '@components/layout/Layout'
import ProductCarousel from '@components/products/ProductCarousel'
import ProductContainer from '@components/products/ProductContainer'
import axios from 'axios'

export const getServerSideProps = async (ctx) => {
  const { data: product } = await axios.get(
    `http://localhost:3000/api/products/${ctx.params.id}`
  )
  const { data: products } = await axios.get(
    `http://localhost:3000/api/products`
  )
  return {
    props: {
      product,
      products,
    },
  }
}

export default function ProductPage({ product, products }) {
  return (
    <Layout>
      <ProductContainer {...product} />
      <div className='wrapper'>
        <h2 className='text-xl tracking-widest my-8'>Productos relacionados</h2>
      </div>
      <ProductCarousel products={products} />
    </Layout>
  )
}
