import { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import axios from 'axios'
import { useUIState, useUIDispatch } from '@hooks/useContextHook'
import { useSnackbar } from 'notistack'

//Components
import ContactInfo from '@components/checkout/ContactInfo'
import OrderSummary from '@components/checkout/OrderSummary'
import Layout from '@components/layout/Layout'

function Checkout() {
  const router = useRouter()
  const { cart: orderItems, user } = useUIState()
  const { removeCart } = useUIDispatch()
  const { enqueueSnackbar, closeSnackbar } = useSnackbar()

  const [email, setEmail] = useState(user?.email || 'buenoct2001@gmail.com')
  const [name, setName] = useState(user?.fullname.split(' ')[0] || 'Carlos')
  const [lastname, setLastname] = useState(
    user?.fullname.split(' ')[1] || 'Bueno'
  )
  const [phone, setPhone] = useState('84465132')
  const [address, setAddress] = useState('mi casa')
  const [reference, setReference] = useState('la esquina')
  const [paymentMethod, setPaymentMethod] = useState('Tarjeta')
  const [province, setProvince] = useState('Santo Domingo')

  useEffect(() => {
    if (!user || orderItems.length == 0) {
      closeSnackbar()
      enqueueSnackbar(
        !user
          ? 'Necesitas iniciar session!'
          : 'Debes de tener productos en el carrito!',
        {
          variant: 'error',
        }
      )
      router.push('/')
    }
  }, [])

  const createOrder = async () => {
    await axios.post('/api/orders', {
      orderItems: orderItems.map((i) => ({ product: i.product, qty: i.qty })),
      contact: {
        user: user.id,
        phone_number: phone,
      },
      shipping: {
        name,
        lastname,
        province,
        address,
        reference,
      },
      totalPrice: 1000,
      paymentMethod,
    })
    removeCart()
  }

  return (
    <Layout>
      <section className='wrapper mb-20 text-gray-900'>
        <div className='flex justify-between items-center w-full relative mb-6'>
          <button
            onClick={() => router.back()}
            className='px-10 py-2 mb-2 bg-slate-200 rounded-md font-medium'
          >
            Atras
          </button>
          <h1 className='absolute top-2/4 left-2/4 -translate-x-2/4 -translate-y-2/4 text-4xl uppercase tracking-wider font-medium'>
            CheckOut
          </h1>
          <p></p>
        </div>
        <div className='grid lg:grid-cols-2 md:gap-x-6 xl:gap-x-20'>
          <ContactInfo />
          <OrderSummary createOrder={createOrder} />
        </div>
      </section>
    </Layout>
  )
}

export default Checkout
