import { PayPalScriptProvider } from '@paypal/react-paypal-js'
import { SnackbarProvider } from 'notistack'
import ContextProvider from '../context'
import '../styles/globals.css'

function MyApp({ Component, pageProps }) {
  return (
    <PayPalScriptProvider
      options={{
        'client-id':
          'AbGBcQDJqVh4EArSj7gcTMJ4KHk-p87_XzxxADnoSHb9W23hTMhQvcj8iOPC3RJIJ5Cqyt4Te6MQX99G',
      }}
    >
      <SnackbarProvider maxSnack={3}>
        <ContextProvider>
          <Component {...pageProps} />
        </ContextProvider>
      </SnackbarProvider>
    </PayPalScriptProvider>
  )
}

export default MyApp
