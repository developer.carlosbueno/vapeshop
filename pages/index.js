import Layout from '@components/layout/Layout'

export default function Home({ orders }) {
  return (
    <Layout>
      <div className='wrapper'>Home</div>
    </Layout>
  )
}
