export const actionTypes = {
  ADD_TO_CART: 'ADD_TO_CART',
  REMOVE_FROM_CART: 'REMOVE_FROM_CART',
  REMOVE_CART: 'REMOVE_CART',
  USER_LOGOUT: 'USER_LOGOUT',
  GET_MY_ORDERS: 'GET_MY_ORDERS',
  GET_MY_ORDER: 'GET_MY_ORDER',
  GET_CATEGORIES: 'GET_CATEGORIES',
}

const reducer = (state, { type, payload }) => {
  switch (type) {
    // CART REDUCER

    case actionTypes.ADD_TO_CART:
      return {
        ...state,
        cart: [...state.cart, payload],
      }

    case actionTypes.REMOVE_FROM_CART:
      const items = state.cart.filter((i) => i.product !== payload)
      return {
        ...state,
        cart: items,
      }

    case actionTypes.REMOVE_CART:
      return {
        ...state,
        cart: [],
      }

    // USER REDUCER

    case actionTypes.USER_LOGOUT:
      return {
        ...state,
        user: null,
      }

    // Order Reducer

    case actionTypes.GET_MY_ORDERS:
      return {
        ...state,
        orders: payload,
      }

    case actionTypes.GET_MY_ORDER:
      return {
        ...state,
        order: payload,
      }

    // Category Reducer

    case actionTypes.GET_CATEGORIES:
      return {
        ...state,
        categories: payload,
      }

    default:
      return state
  }
}

export default reducer
