import { createContext, useReducer, useEffect } from 'react'
import cookies from 'js-cookie'
import reducer from './reducer'

const initialContext = {
  cart: cookies.get('cartItems') ? JSON.parse(cookies.get('cartItems')) : [],
  user: cookies.get('userInfo') ? JSON.parse(cookies.get('userInfo')) : null,
  orders: [],
  order: {},
}

export const StateContext = createContext(initialContext)
export const DispatchContext = createContext(undefined)

export default function ContextProvider({ children }) {
  const [state, dispatch] = useReducer(reducer, initialContext)

  return (
    <StateContext.Provider value={state}>
      <DispatchContext.Provider value={dispatch}>
        {children}
      </DispatchContext.Provider>
    </StateContext.Provider>
  )
}
