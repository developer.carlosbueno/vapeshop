import { useContext, useCallback, useMemo } from 'react'
import { StateContext, DispatchContext } from '../context'
import { actionTypes } from '../context/reducer'
import cookies from 'js-cookie'
import axios from 'axios'

export const useUIState = () => {
  return useContext(StateContext)
}

export const useUIDispatch = () => {
  const dispatch = useContext(DispatchContext)
  const { cart } = useUIState()

  if (dispatch === undefined) {
    throw new Error('useBookingDispatch must be used within a BookingProvider')
  }

  const addToCart = useCallback(
    (payload) => {
      dispatch({ type: actionTypes.ADD_TO_CART, payload })
      cookies.set('cartItems', JSON.stringify([...cart, payload]))
    },
    [dispatch]
  )

  const removeFromCart = useCallback(
    (payload) => {
      dispatch({ type: actionTypes.REMOVE_FROM_CART, payload })
      cookies.set(
        'cartItems',
        JSON.stringify(cart.filter((i) => i.product !== payload))
      )
    },
    [dispatch]
  )

  const removeCart = useCallback(() => {
    dispatch({ type: actionTypes.REMOVE_CART })
    cookies.remove('cartItems')
  }, [dispatch])

  const logout = useCallback(() => {
    dispatch({ type: actionTypes.USER_LOGOUT })
    cookies.remove('userInfo')
  }, [dispatch])

  const myOrders = useCallback(async (id, token) => {
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
    const { data: payload } = await axios.get(`/api/orders/user/${id}`, config)
    dispatch({ type: actionTypes.GET_MY_ORDERS, payload })
  })

  const getMyOrder = useCallback(async (id, token) => {
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
    const { data: payload } = await axios.get(`/api/orders/${id}`, config)
    dispatch({ type: actionTypes.GET_MY_ORDER, payload })
  })

  const getCategories = useCallback(async () => {
    const { data: payload } = await axios.get(`/api/categories`)
    dispatch({ type: actionTypes.GET_CATEGORIES, payload })
  })

  return useMemo(
    () => ({
      addToCart,
      removeFromCart,
      logout,
      myOrders,
      getMyOrder,
      removeCart,
      getCategories,
    }),
    [dispatch]
  )
}
