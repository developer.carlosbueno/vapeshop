import passport from 'passport'
import User from '@models/users'
import { sign } from '@utils/jwt'
import { Strategy as GoogleStrategy } from 'passport-google-oauth2'

passport.use(
  'google',
  new GoogleStrategy(
    {
      clientID: process.env.GOOGLE_ID_CLIENT,
      clientSecret: process.env.GOOGLE_SECRET_CLIENT,
      callbackURL: 'http://localhost:3000/api/google/callback',
      passReqToCallback: true,
    },
    async (request, accessToken, refreshToken, profile, done) => {
      try {
        const user = await User.findOne({ email: profile.email })
        if (!user) {
          const newUser = new User({
            fullname: profile.displayName,
            email: profile.email,
            accessToken,
          })
          await newUser.save()
          let token = sign(newUser)
          newUser.token = token
          await new User.save()
          return done(null, newUser, { message: 'Succesfully auth', token })
        } else {
          let token = sign(user)
          user.token = token
          await user.save()
          return done(null, user, { message: 'Succesfully auth', token })
        }
      } catch (error) {
        console.log(error)
        return done(error, false, { message: 'Google auth internal error' })
      }
    }
  )
)
