export const categories = [
  { title: 'desechables' },
  { title: 'pods' },
  { title: 'tanques' },
  { title: 'liquidos' },
  { title: 'accesorios' },
  { title: 'bateria' },
]

export const brands = [
  { title: 'swift' },
  { title: 'smoke' },
  { title: 'voopoo' },
  { title: 'magi' },
]

export const products = [
  {
    title: 'Novo',
    description: 'Pequeño, buena autonimia y gran sabor',
    price: 1200,
    image: 'gmail.com',
    stock: 25,
    category: '62e62868e0fb26203d6a7b48',
    brand: '62e62868e0fb26203d6a7b4f',
  },
  {
    title: 'Swift',
    description: 'El desechable mas demandado de la epoca',
    price: 950,
    image: 'gmail.com',
    stock: 60,
    category: '62e62868e0fb26203d6a7b47',
    brand: '62e62868e0fb26203d6a7b4e',
  },
  {
    title: 'Nord 2',
    description: 'Potente peligro agresivo y sale malo',
    price: 1500,
    image: 'gmail.com',
    stock: 12,
    category: '62e62868e0fb26203d6a7b48',
    brand: '62e62868e0fb26203d6a7b4f',
  },
  {
    title: '25ml x 75ml',
    description: 'Caoba de alaska',
    price: 2000,
    image: 'gmail.com',
    stock: 12,
    category: '62e62868e0fb26203d6a7b49',
    brand: '62e62868e0fb26203d6a7b4f',
  },
  {
    title: 'Novo',
    description: 'Caoba de alaska',
    price: 2000,
    image: 'gmail.com',
    stock: 12,
    category: '62e62868e0fb26203d6a7b4a',
    brand: '62e62868e0fb26203d6a7b50',
  },
  {
    title: 'Novo',
    description: 'Caoba de alaska',
    price: 2000,
    image: 'gmail.com',
    stock: 12,
    category: '62e62868e0fb26203d6a7b47',
    brand: '62e62868e0fb26203d6a7b4f',
  },
]
