export default function success(res, status, body) {
  return res.status(status).json(body)
}
