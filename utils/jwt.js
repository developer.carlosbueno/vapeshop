import jwt from 'jsonwebtoken'

export const sign = (user) => {
  return jwt.sign({ id: user._id }, process.env.JWT_SECRET)
}
