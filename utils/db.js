import mongoose from 'mongoose'
import '@models/products'

const connection = {}

export async function connect() {
  if (connection.isConnected) {
    console.log('DB already connected')
    return
  }

  if (mongoose.connections.length > 0) {
    connection.isConnected = mongoose.connections[0].readyState
    if (connection.isConnected === 1) {
      console.log('Connected to previus connection')
      return
    }

    await mongoose.disconnect()
  }

  const db = await mongoose.connect(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  console.log('New db connection')
  connection.isConnected = db.connections[0].readyState
}

export async function disconnect() {
  if (connection.isConnected) {
    if (process.env.NODE_ENV === 'production') {
      await mongoose.disconnect()
      connection.isConnected = false
    } else {
      console.log('not disconnected')
    }
  }
}
