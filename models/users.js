import mongoose, { model, Schema } from 'mongoose'

const userSchema = new Schema(
  {
    fullname: String,
    email: String,
    accessToken: String,
    token: String,
  },
  {
    timestamps: true,
  }
)

const User = mongoose.models.users || model('users', userSchema)

export default User
