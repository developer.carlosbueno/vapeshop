import mongoose, { Schema, model } from 'mongoose'

const orderSchema = new Schema(
  {
    orderItems: [
      {
        product: {
          type: Schema.Types.ObjectId,
          ref: 'products',
        },
        qty: Number,
      },
    ],
    contact: {
      user: {
        type: Schema.Types.ObjectId,
        ref: 'users',
      },
      phone_number: Number,
    },
    shipping: {
      name: String,
      lastname: String,
      province: String,
      address: String,
      reference: String,
    },
    totalPrice: Number,
    paymentMethod: String,
    state: String,
  },
  {
    timestamps: true,
  }
)

mongoose.set('strictPopulate', false)

const Order = mongoose.models.orders || model('orders', orderSchema)

export default Order
