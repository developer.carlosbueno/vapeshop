import mongoose, { Schema, model } from 'mongoose'

const categorySchema = new Schema({
  title: String,
})

const brandSchema = new Schema({
  title: String,
})

const productSchema = new Schema({
  title: String,
  price: Number,
  description: String,
  image: String,
  stock: Number,
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'categories',
  },
  brand: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'brands',
  },
})

const Product = mongoose.models.products || model('products', productSchema)

export const Brand = mongoose.models.brands || model('brands', brandSchema)

export const Category =
  mongoose.models.categories || model('categories', categorySchema)

export default Product
