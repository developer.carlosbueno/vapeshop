import { useState, useEffect } from 'react'
import { useUIDispatch, useUIState } from '@hooks/useContextHook'

//Components
import OrdersModal from '@components/modal/orders/OrdersModal'
import OrderCard from '@components/OrderCard'

function Profile({ open }) {
  const { logout } = useUIDispatch()
  const { user, orders } = useUIState()
  const [showModal, setShowModal] = useState(false)

  const { myOrders, getMyOrder } = useUIDispatch()

  useEffect(() => {
    myOrders(user.id, user.token)
  }, [])

  const getOrder = (id, token) => {
    getMyOrder(id, token)
  }

  return (
    <div
      className={`w-full md:absolute z-50 -left-60 top-8 overflow-hidden bg-white md:w-72 text-center transition-all duration-300 rounded-lg ${
        open ? 'h-fit border' : 'h-0 border-none'
      }`}
    >
      <h2 className='text-xl font-bold py-4 border-b'>{user.fullname}</h2>
      <div className='m-auto mb-6 mt-2 px-4'>
        <p
          className='text-sm underline text-blue-600 mb-2'
          onClick={() => setShowModal(true)}
        >
          Historial
        </p>
        {orders ? (
          orders
            ?.filter((o) => o.state == 'pendiente')
            .map((o) => (
              <OrderCard
                key={o._id}
                getOrder={() => getOrder(o._id, user.token)}
              />
            ))
        ) : (
          <p className='tracking-widest text-lg'>No tienes ordenes!</p>
        )}
      </div>
      <h4
        onClick={logout}
        className='text-sm text-red-600 py-2 border-t border-red-200 transition-all hover:text-white hover:bg-red-500'
      >
        Cerrar Sesion
      </h4>
      <OrdersModal show={showModal} onClose={() => setShowModal(false)} />
    </div>
  )
}

export default Profile
