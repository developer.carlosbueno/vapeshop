import React from 'react'

//Components
import FacebookSignIn from './FacebookSignIn'
import GoogleSignIn from './GoogleSignIn'

function SignIn({ open }) {
  return (
    <div
      className={`w-full md:absolute z-50 -left-60 top-8 overflow-hidden bg-white md:w-72 text-center transition-all duration-300 rounded-lg ${
        open ? 'h-fit border' : 'h-0 border-none'
      }`}
    >
      <h2 className='text-xl font-bold py-4 border-b mb-4'>Inicia Sesion</h2>
      <div className='m-auto w-fit'>
        <GoogleSignIn />
        {/* <FacebookSignIn /> */}
      </div>
    </div>
  )
}

export default SignIn
