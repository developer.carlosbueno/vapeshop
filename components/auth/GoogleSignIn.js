import Link from 'next/link'

// Icons
import { FcGoogle } from 'react-icons/fc'

function GoogleSignIn() {
  return (
    <Link href='/api/google' passHref>
      <a className='loginButton'>
        <FcGoogle size={30} />
        <p className='text-lg font-semibold ml-4'>Sign In With Google</p>
      </a>
    </Link>
  )
}

export default GoogleSignIn
