//Icons
import { FaFacebook } from 'react-icons/fa'

function FacebookSignIn() {
  return (
    <button className='loginButton'>
      <FaFacebook className='text-blue-500' size={30} />
      <p className='text-lg font-semibold ml-4'>Sign In With Facebook</p>
    </button>
  )
}

export default FacebookSignIn
