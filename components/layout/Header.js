import { useEffect, useState } from 'react'
import dynamic from 'next/dynamic'
import Link from 'next/link'
import { useUIState, useUIDispatch } from '@hooks/useContextHook'

//Components
import MobileMenu from './MobileMenu'
import SignIn from '@components/auth/SignIn'
import Profile from '@components/auth/Profile'

//Icons
import { ImSearch } from 'react-icons/im'
import { FaUserAlt } from 'react-icons/fa'
import { HiShoppingCart, HiMenu } from 'react-icons/hi'

const Cart = dynamic(() => import('@components/cart/Cart'), {
  ssr: false,
})

function Header({}) {
  const { user, categories } = useUIState()
  const [menu, setMenu] = useState(false)
  const [cart, setCart] = useState(false)
  const [signIn, setSignIn] = useState(false)

  const { getCategories } = useUIDispatch()

  useEffect(() => {
    if (!categories) {
      getCategories()
    }
  }, [])

  return (
    <>
      <div className='border-y-2 border-gray-500 py-3 text-center'>
        <h2 className='font-bold text-xs lg:text-lg'>
          WARNING: This product contains nicotine. Nicotine is an addictive
          chemical.
        </h2>
      </div>
      <header className='wrapper'>
        <div
          className='grid mt-2 py-6 border-b-2 border-gray-500 mb-4'
          style={{
            gridTemplateColumns: '80px auto 100px',
          }}
        >
          <div className='cursor-pointer'>
            <HiMenu
              size={25}
              className='block md:hidden'
              onClick={() => setMenu(true)}
            />
          </div>
          <Link href='/' passHref>
            <a className='text-center sm:text-lg md:text-2xl lg:text-4xl '>
              VAPING CHILLER
            </a>
          </Link>
          <ul className='flex justify-end items-center lg:text-xl'>
            <li className='cursor-pointer hidden lg:block'>
              <ImSearch />
            </li>
            <li className='relative px-6 cursor-pointer hidden md:block'>
              <FaUserAlt onClick={() => setSignIn(!signIn)} />
              {!user ? (
                <SignIn open={signIn} />
              ) : (
                <Profile open={signIn} user={user} />
              )}
            </li>
            <li className='cursor-pointer' onClick={() => setCart(true)}>
              <HiShoppingCart className='text-xl lg:text-2xl' />
            </li>
          </ul>
        </div>

        {/* Navbar */}
        <nav className='hidden md:block mb-10'>
          <ul className='uppercase flex justify-center items-center text-sm lg:text-md font-bold m-auto'>
            {categories?.map((c) => (
              <li className='menuListItem'>
                <Link href={`/products?category=${c.title}`} passHref>
                  <a>{c.title}</a>
                </Link>
              </li>
            ))}
          </ul>
        </nav>
      </header>
      {/* Mobile Menu */}
      <MobileMenu setMenu={setMenu} menu={menu} />
      <Cart cart={cart} setCart={setCart} />
    </>
  )
}

export default Header
