import { useState } from 'react'
import Link from 'next/link'
import Image from 'next/image'
import { useUIState } from '@hooks/useContextHook'

import SignIn from '@components/auth/SignIn'
import Profile from '@components/auth/Profile'

function MobileMenu({ setMenu, menu }) {
  const [account, setAccount] = useState(false)

  return (
    <div
      className={`fixed transition-all top-0 bottom-0 z-50 bg-white w-screen h-screen md:hidden ${
        menu ? 'left-0' : '-left-full'
      }`}
    >
      <div className='grid grid-cols-2 relative h-16 text-lg uppercase font-bold'>
        <h1
          onClick={() => setAccount(false)}
          className={`mobileMenuButton ${
            !account ? 'bg-white text-gray-800' : 'bg-gray-800 text-white'
          }`}
        >
          Menu
        </h1>
        <h1
          onClick={() => setAccount(true)}
          className={`mobileMenuButton ${
            account ? 'bg-white text-gray-800' : 'bg-gray-800 text-white'
          }`}
        >
          Account
        </h1>
        <button
          onClick={() => setMenu(false)}
          className='h-8 w-8 absolute bg-white shadow-lg rounded-full top-2/4 left-2/4 -translate-x-1/2 -translate-y-1/2'
        >
          x
        </button>
      </div>
      {!account ? <MenuList onClose={() => setMenu(false)} /> : <AccountList />}
    </div>
  )
}

const MenuList = ({ onClose }) => {
  const { categories } = useUIState()

  return (
    <ul>
      {categories?.map((c) => (
        <li onClick={onClose}>
          <Link href={`/products?category=${c.title}`} passHref>
            <a className='mobileListItem'>
              <h3>{c.title}</h3>
              <Image
                width={50}
                height={50}
                src={
                  'https://www.elementvape.com/media/Dckap/menu-level1/Mobile_Menu_Category_BRANDS_70x62.png'
                }
              />
            </a>
          </Link>
        </li>
      ))}
    </ul>
  )
}

const AccountList = () => {
  const { user } = useUIState()
  return <>{!user ? <SignIn open={true} /> : <Profile open={true} />}</>
}

export default MobileMenu
