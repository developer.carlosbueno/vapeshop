import { useState } from 'react'
import dynamic from 'next/dynamic'

//Components
import ProductCard from './ProductCard'

// Dynamic
const ProductsFilter = dynamic(
  () => import('@components/products/ProductsFilter'),
  {
    ssr: false,
  }
)

//Icons
import { FaFilter } from 'react-icons/fa'

function ProductsContainer({ products }) {
  const [filterMenu, setFilterMenu] = useState(false)

  return (
    <div
      className='wrapper lg:grid -z-50'
      style={{
        gridTemplateColumns: '300px 1fr',
        marginTop: '50px',
      }}
    >
      {/* Filters */}
      <ProductsFilter filterMenu={filterMenu} setFilterMenu={setFilterMenu} />
      {/* ProductList */}
      <div className='px-2 w-full'>
        <div className='flex justify-between items-center mb-2 py-2 border-b-2'>
          <p className='font-medium text-gray-500 text-sm'>Total: 31</p>
          <div className='flex items-center'>
            <select className='border-2 rounded-md mr-4 text-sm w-24'>
              <option>Precio</option>
              <option>Mas precios</option>
              <option>Menos precios</option>
            </select>
            <select className='border-2 rounded-md mr-4 text-sm w-24'>
              <option>Novedad</option>
              <option>Mas precios</option>
              <option>Menos precios</option>
            </select>
            <FaFilter
              className='text-gray-700 cursor-pointer lg:hidden'
              onClick={() => setFilterMenu(true)}
            />
          </div>
        </div>
        <div className='grid grid-cols-2 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-10'>
          {products?.map((product) => (
            <ProductCard key={product._id} {...product} />
          ))}
        </div>
      </div>
    </div>
  )
}

export default ProductsContainer
