import { useState } from 'react'
import Image from 'next/image'
import { useSnackbar } from 'notistack'
import { useUIDispatch } from '@hooks/useContextHook'

function ProductContainer({ title, description, price, stock, image, _id }) {
  const { addToCart } = useUIDispatch()
  const { enqueueSnackbar, closeSnackbar } = useSnackbar()

  const [qty, setQty] = useState(1)

  const changeQty = (decrease = false) => {
    if (decrease) {
      setQty((prev) => (prev === 1 ? prev : Number(prev) - 1))
    } else {
      setQty((prev) => Number(prev) + 1)
    }
  }

  const handleAddToCart = () => {
    closeSnackbar()
    addToCart({ title, price, qty, product: _id })
    enqueueSnackbar('Producto añadido al carrito!.', {
      variant: 'success',
    })
  }

  return (
    <div className='wrapper my-10 md:grid md:grid-cols-2'>
      {/* Image */}
      <div>
        <Image
          width={500}
          height={400}
          layout='fixed'
          className='m-auto max-h-[60vh]'
          src='https://www.elementvape.com/media/catalog/product/cache/177ff88e405dbdaf9e02a16630acb6f3/v/o/voopoo_-_argus_pod_cartridge_-_accessories_-_packaging.png'
        />
      </div>
      {/* Info */}
      <div>
        <h1 className='text-center text-xl mb-4 md:text-left lg:text-3xl'>
          {title}
        </h1>
        <div className=' flex justify-between items-center border-y-2 py-4'>
          <h2 className='font-bold text-xl tracking-wider'>${price}</h2>
          <p className={`${stock > 0 ? 'text-green-600' : 'text-red-500'}`}>
            {stock > 0 ? 'In Stock' : 'Out Stock'}
          </p>
        </div>
        <div className='my-2'>
          <h3 className='font-medium mb-2'>Informacion</h3>
          <p>{description}</p>
        </div>
        <div className='my-4 text-center'>
          <h3 className='uppercase font-medium'>Qty</h3>
          <div className='flex border-2 w-fit m-auto'>
            <button
              className='px-4 py-2 grid place-items-center'
              onClick={() => changeQty(true)}
            >
              -
            </button>
            <input
              type='number'
              className='outline-none text-center w-24 bg-transparent'
              value={qty}
              onChange={(e) => setQty(e.target.value)}
            />
            <button
              className='px-4 py-2 grid place-items-center'
              onClick={() => changeQty(false)}
            >
              +
            </button>
          </div>
        </div>
        <button
          onClick={handleAddToCart}
          className='w-full py-4 rounded-md bg-yellow-400'
        >
          Add to cart
        </button>
      </div>
    </div>
  )
}

export default ProductContainer
