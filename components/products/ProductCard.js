import { useRouter } from 'next/router'
import Image from 'next/image'
import { useUIDispatch } from '@hooks/useContextHook'
import { useSnackbar } from 'notistack'

// Icons
import { BsFillCartPlusFill } from 'react-icons/bs'

function ProductCard({ carousel, title, price, _id }) {
  const router = useRouter()
  const { addToCart } = useUIDispatch()
  const { enqueueSnackbar, closeSnackbar } = useSnackbar()

  const handleAddToCart = () => {
    closeSnackbar()
    addToCart({ title, price, qty: 1, product: _id })
    enqueueSnackbar('Producto añadido al carrito!.', {
      variant: 'success',
    })
  }

  return (
    <div className={`productCard ${carousel && 'w-72'} `}>
      <button onClick={handleAddToCart} className='productCardButton z-10'>
        <BsFillCartPlusFill size={20} />
      </button>
      <div
        onClick={() => router.push(`/products/${_id}`)}
        className='text-center'
      >
        <Image
          src={
            'https://www.elementvape.com/media/catalog/product/cache/177ff88e405dbdaf9e02a16630acb6f3/v/o/voopoo_-_argus_pod_cartridge_-_accessories_-_packaging.png'
          }
          width={200}
          height={220}
        />
        <div className='text-center w-3/4 m-auto mt-2'>
          <h2>{title}</h2>
          <p className='font-bold font-poppins'>${price}</p>
        </div>
      </div>
    </div>
  )
}

export default ProductCard
