import React from 'react'
import ProductCard from './ProductCard'

function ProductCarousel({ products }) {
  return (
    <div className='wrapper'>
      <div className='carousel-wrapper'>
        {products.map((p) => (
          <div key={p._id} className='item'>
            <ProductCard carousel {...p} />
          </div>
        ))}
      </div>
    </div>
  )
}

export default ProductCarousel
