import React from 'react'

function ProductsFilter({ filterMenu, setFilterMenu }) {
  return (
    <div
      className='fixed top-0 bottom-0 border-l-2 lg:border-none w-0 right-0 transition-all bg-white z-40 overflow-hidden lg:w-full lg:block lg:relative'
      style={{
        width: filterMenu && `${window.innerWidth < 600 ? '100%' : '400px'}`,
        padding: filterMenu ? '8px' : '0px',
      }}
    >
      <div className='flex justify-between items-center'>
        <h1 className='text-xl font-bold tracking-widest mb-4'>Marcas</h1>
        <button onClick={() => setFilterMenu(false)} className='lg:hidden'>
          X
        </button>
      </div>
      <ul>
        <li className='border-2 rounded-md p-2 mb-2 cursor-pointer text-sm font-medium text-gray-600 uppercase'>
          Smoke
        </li>
        <li className='border-2 rounded-md p-2 mb-2 cursor-pointer text-sm font-medium text-gray-600 uppercase'>
          Smoke
        </li>
        <li className='border-2 rounded-md p-2 mb-2 cursor-pointer text-sm font-medium text-gray-600 uppercase'>
          Smoke
        </li>
      </ul>
    </div>
  )
}

export default ProductsFilter
