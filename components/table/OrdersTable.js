// Icons
import OrderModal from '@components/modal/order/OrderModal'
import { useState } from 'react'
import { AiOutlineSearch } from 'react-icons/ai'
import { useUIState, useUIDispatch } from '@hooks/useContextHook'

function OrdersTable({
  orders,
  handleChangeSelected,
  selected,
  handleSendOrder,
}) {
  const [show, setShow] = useState(false)
  const { user } = useUIState()
  const { getMyOrder } = useUIDispatch()

  const handleOpenOrder = (order_id) => {
    getMyOrder(order_id, user.token)
    setShow(true)
  }

  return (
    <div
      className='relative overflow-x-auto overflow-y-auto not-scroll'
      style={{
        height: '480px',
      }}
    >
      <div className='py-2'>
        <label htmlFor='table-search' className='sr-only'>
          Search
        </label>
        {/* Header */}
        <div className='flex justify-between'>
          {/* Input */}
          <div className='relative mt-1'>
            <div className='absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none'>
              <AiOutlineSearch className='w-5 h-5 text-gray-500 dark:text-gray-400' />
            </div>
            <input
              type='text'
              id='table-search'
              className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-80 pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500'
              placeholder='Search for items'
            />
          </div>
          {/* Filters */}
          <div className='flex'>
            <select className='input mr-4'>
              <option>Pago</option>
            </select>

            <select className='input'>
              <option>Pago</option>
            </select>
            {selected.length > 0 ? (
              <button
                onClick={handleSendOrder}
                className='bg-green-400 px-8 rounded-md ml-2'
              >
                Enviar
              </button>
            ) : null}
          </div>
        </div>
      </div>
      <table className='w-full overflow-y-scroll rounded-lg text-sm text-left text-gray-500 dark:text-gray-400 '>
        <thead className='text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400'>
          <tr>
            <th scope='col' className='p-4'>
              <div className='flex items-center'>
                <input
                  id='checkbox-all-search'
                  type='checkbox'
                  className='w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600'
                />
                <label htmlFor='checkbox-all-search' className='sr-only'>
                  checkbox
                </label>
              </div>
            </th>
            <th scope='col' className='px-6 py-3'>
              id
            </th>
            <th scope='col' className='px-6 py-3'>
              Usuario
            </th>
            <th scope='col' className='px-6 py-3'>
              Products
            </th>
            <th scope='col' className='px-6 py-3'>
              Provincia
            </th>
            <th scope='col' className='px-6 py-3'>
              Estado
            </th>
            <th scope='col' className='px-6 py-3'>
              Metodo de pago
            </th>
            <th scope='col' className='px-6 py-3'></th>
          </tr>
        </thead>
        <tbody>
          {orders?.map((order) => (
            <tr
              key={order._id}
              className='bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600'
            >
              <td className='w-4 p-4'>
                <div className='flex items-center'>
                  <input
                    onChange={(e) => handleChangeSelected(e, order._id)}
                    type='checkbox'
                    className='w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600'
                  />
                  <label htmlFor='checkbox-table-search-1' className='sr-only'>
                    checkbox
                  </label>
                </div>
              </td>
              <th
                scope='row'
                className='px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap'
              >
                {order._id}
              </th>
              <td className='px-6 py-4'>{`${order.shipping.name} ${order.shipping.lastname}`}</td>
              <td className='px-6 py-4'>{order.orderItems.length}</td>
              <td className='px-6 py-4'>{order.shipping.province}</td>
              <td className='px-6 py-4 capitalize'>{order.state}</td>
              <td className='px-6 py-4 text-center'>{order.paymentMethod}</td>
              <td className='px-6 py-4 text-center'>
                <button
                  onClick={() => handleOpenOrder(order._id)}
                  className='font-medium text-blue-600 dark:text-blue-500 hover:underline'
                >
                  ver
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <OrderModal show={show} onClose={() => setShow(false)} />
    </div>
  )
}

export default OrdersTable
