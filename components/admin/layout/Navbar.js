import Link from 'next/link'

// Icons
import { HiMenu } from 'react-icons/hi'
import { BiLogOut } from 'react-icons/bi'
import { FaBoxOpen } from 'react-icons/fa'
import { RiShoppingBagFill, RiChatHistoryFill } from 'react-icons/ri'
import { AiOutlineCloseCircle, AiFillDashboard } from 'react-icons/ai'

function Navbar({ open, setOpen }) {
  return (
    <div
      className={`fixed flex flex-col justify-between py-4 px-2 bg-slate-500 text-white transition-all -left-full h-screen md:-left-0 md:w-20 ${
        open ? '-left-0 : right-0 md:w-80' : 'w-20'
      }`}
    >
      {/* Header */}
      <div className='text-5xl flex justify-between items-center mb-6'>
        {!open ? (
          <HiMenu className='w-full' onClick={() => setOpen(true)} />
        ) : (
          <>
            <h1 className='text-2xl font-medium'>Vaping Chiller</h1>
            <AiOutlineCloseCircle onClick={() => setOpen(false)} />
          </>
        )}
      </div>

      {/* List */}
      <ul className='h-full'>
        <li>
          <Link href='/admin/dashboard' passHref>
            <a
              className={`navbar_item ${open ? 'px-4 py-2' : 'justify-center'}`}
            >
              <AiFillDashboard className={`text-5xl ${open && 'mr-2'}`} />
              {open && <p>Dashboard</p>}
              {!open && (
                <span className='navbar_helper -right-32'>Dashboard</span>
              )}
            </a>
          </Link>
        </li>

        <li>
          <Link href='/admin/pedidos' passHref>
            <a
              className={`navbar_item ${open ? 'px-4 py-2' : 'justify-center'}`}
            >
              <RiShoppingBagFill className={`text-5xl ${open && 'mr-2'}`} />
              {open && <p>Ventas</p>}
              {!open && <span className='navbar_helper -right-24'>Ventas</span>}
            </a>
          </Link>
        </li>

        <li className={`navbar_item ${open ? 'px-4 py-2' : 'justify-center'}`}>
          <RiChatHistoryFill className={`text-5xl ${open && 'mr-2'}`} />
          {open && <p>Historial</p>}
          {!open && <span className='navbar_helper -right-28'>Historial</span>}
        </li>

        <li className={`navbar_item ${open ? 'px-4 py-2' : 'justify-center'}`}>
          <FaBoxOpen className={`text-5xl ${open && 'mr-2'}`} />
          {open && <p>Inventario</p>}
          {!open && <span className='navbar_helper -right-32'>Inventario</span>}
        </li>
      </ul>

      {/* Footer */}
      <div
        className={`navbar_item mb-0 ${open ? 'px-4 py-2' : 'justify-center'}`}
      >
        <BiLogOut className={`text-5xl ${open && 'mr-2'}`} />
        {open && <p>LogOut</p>}
        {!open && <span className='navbar_helper -right-24'>LogOut</span>}
      </div>
    </div>
  )
}

export default Navbar
