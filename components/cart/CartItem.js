import Image from 'next/image'
import { useSnackbar } from 'notistack'
import { useUIDispatch } from '@hooks/useContextHook'

function CartItem({ title, price, qty, product }) {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar()
  const { removeFromCart } = useUIDispatch()
  const handleRemoveFromCart = () => {
    closeSnackbar()
    removeFromCart(product)
    enqueueSnackbar('Producto removido del carrito!.', {
      variant: 'success',
    })
  }

  return (
    <div className='my-4 flex border-y-2'>
      <Image
        src={
          'https://www.elementvape.com/media/catalog/product/cache/177ff88e405dbdaf9e02a16630acb6f3/s/m/smok_-_novo_4_mini_-_pod_system_-_green_blue_cobra_1.png'
        }
        width={120}
        height={120}
        className='mr-2'
      />
      <div className='w-full flex flex-col justify-between py-2'>
        <div className='flex justify-between'>
          <h2>{title}</h2>
          <p className='font-bold'>${price}</p>
        </div>
        <div className='flex justify-between'>
          <input className='' value={qty} />
          <div
            onClick={handleRemoveFromCart}
            className='w-5 h-5 grid place-items-center text-xs font-bold rounded-full bg-red-300'
          >
            x
          </div>
        </div>
      </div>
    </div>
  )
}

export default CartItem
