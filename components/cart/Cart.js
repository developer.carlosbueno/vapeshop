import Link from 'next/link'
import { useUIState } from '@hooks/useContextHook'

//Components
import CartItem from './CartItem'

function Cart({ cart, setCart }) {
  const { cart: cartItems } = useUIState()

  return (
    <div>
      <div
        onClick={() => setCart(false)}
        className={`cart_overlay ${!cart && 'hidden'}`}
      ></div>
      <div
        className={`cartContainer ${cart && 'active'}`}
        style={{
          width: cart
            ? `${window.innerWidth < 500 ? '100vw' : '400px'}`
            : '0px',
          padding: cart && '10px 20px',
        }}
      >
        {/* Header */}
        <div className='flex justify-between items-center mt-6'>
          <h3 className='font-bold tracking-wider uppercase'>Carrito</h3>
          <button onClick={() => setCart(false)}>X</button>
        </div>
        {/* Body */}
        <div
          className='grid h-full'
          style={{
            gridTemplateRows: '1fr 200px',
          }}
        >
          {/* Cart Items */}
          <div className='h-full border-b-2 overflow-y-scroll not-scroll'>
            {cartItems?.map((i) => (
              <CartItem key={i.product} {...i} />
            ))}
          </div>

          {/* CartInfo */}
          <div className='h-full mt-2'>
            <div className='flex justify-between'>
              <h3>Subtotal</h3>
              <p>$20</p>
            </div>
            <p className='text-xs text-gray-500'>
              Shipping e impuestos se calculan en el checkout.
            </p>
            <Link href='/checkout' passHref>
              <a className='w-full h-12 mt-4 tracking-widest rounded-xl grid place-items-center border-2 border-green-300 text-green-400 uppercase hover:bg-green-300 hover:text-white'>
                Checkout
              </a>
            </Link>
            <a className='w-full text-center text-sm text-gray-500 mt-2'>
              Continua comprando
            </a>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Cart
