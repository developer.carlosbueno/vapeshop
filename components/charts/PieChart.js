import React, { useState, useEffect } from 'react'
import { Chart as ChartJS, Tooltip, ArcElement, Legend } from 'chart.js'
import { Pie } from 'react-chartjs-2'

ChartJS.register(ArcElement, Tooltip, Legend)

const BarChart = () => {
  var data = {
    labels: ['Efectivo', 'Tarjeta', 'Trasnferencia'],
    datasets: [
      {
        label: 'Provincias',
        data: [300, 50, 100],
        backgroundColor: [
          'rgb(255, 99, 132)',
          'rgb(54, 162, 235)',
          'rgb(255, 205, 86)',
        ],
        hoverOffset: 2,
      },
    ],
  }

  var options = {
    maintainAspectRatio: false,
    scales: {},
    legend: {
      labels: {
        fontSize: 25,
      },
    },
  }

  return (
    <div>
      <Pie data={data} height={400} width={500} options={options} />
    </div>
  )
}

export default BarChart
