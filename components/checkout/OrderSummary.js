import { PayPalButtons } from '@paypal/react-paypal-js'
import { useRouter } from 'next/router'
import { useUIState } from '@hooks/useContextHook'

// Components
import OrderItems from '@components/modal/order/OrderItem'

function OrderSummary({ createOrder }) {
  const router = useRouter()

  const { cart: cartItems } = useUIState()

  return (
    <div>
      <div className='grid p-5 lg:grid-cols-1 lg:grid-rows-2 xl:gap-x-4 md:gap-x-6 bg-white shadow-lg rounded-3xl'>
        {cartItems?.map((i) => (
          <OrderItems key={i.product} {...i} />
        ))}
        <ul className='mt-5'>
          <li className='grid grid-cols-2 w-full mb-5'>
            <h4>Subtotal</h4>
            <h4 className=' text-right'>$1,200</h4>
          </li>
          <li className='grid grid-cols-2 w-full mb-5'>
            <h4>Shipping</h4>
            <h4 className=' text-right'>$200</h4>
          </li>
        </ul>
        <ul className='px-2 mb-5 border-b-2'>
          <li className='grid grid-cols-2 w-full mb-5'>
            <h4 className=' text-2xl'>Total</h4>
            <h4 className=' text-2xl text-right'>$1,400</h4>
          </li>
        </ul>
        <button className='w-full py-3 rounded-md text-center mt-2 bg-blue-400'>
          Pagar ahora
        </button>
        <PayPalButtons
          createOrder={(data, actions) => {
            return actions.order.create({
              purchase_units: [
                {
                  amount: {
                    value: '1.99',
                  },
                },
              ],
            })
          }}
          onApprove={(data, actions) => {
            return actions.order.capture().then((details) => {
              createOrder()
              router.push('/')
            })
          }}
        />
      </div>
    </div>
  )
}

export default OrderSummary
