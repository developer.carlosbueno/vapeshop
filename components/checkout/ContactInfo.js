import { useState } from 'react'
import { useUIState } from '@hooks/useContextHook'
import axios from 'axios'

function ContactInfo() {
  const { user } = useUIState()

  const [email, setEmail] = useState(user?.email || 'buenoct2001@gmail.com')
  const [name, setName] = useState(user?.fullname.split('')[0] || 'Carlos')
  const [lastname, setLastname] = useState(
    user?.fullname.split('')[1] || 'Bueno'
  )
  const [phone, setPhone] = useState('84465132')
  const [address, setAddress] = useState('mi casa')
  const [reference, setReference] = useState('la esquina')
  const [paymentMethod, setPaymentMethod] = useState('Tarjeta')
  const [province, setProvince] = useState('Santo Domingo')

  return (
    <div className='w-full xl:ml-auto h-fit'>
      <div className='w-full mb-10 border-b-2 border-gray-200'>
        <div>
          <h1 className=' lg:text-1xl font-light tracking-tight text-gray-900 sm:text-3xl mb-5'>
            Contact Information
          </h1>
        </div>
        <div className='form-group mb-6 '>
          <input
            type='email'
            className='input'
            id='email'
            placeholder='Email address'
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className='form-group mb-6 '>
          <input
            type='phone'
            className='input'
            placeholder='Phone Number'
            value={phone}
            onChange={(e) => setPhone(e.target.value)}
          />
        </div>
      </div>
      <div className='mb-10 border-b-2 border-gray-200'>
        <div className='lg:pr-8'>
          <h1 className=' lg:text-1xl font-light tracking-tight text-gray-900 sm:text-3xl mb-5'>
            Shipping Address
          </h1>
        </div>
        <div className='grid grid-cols-2 gap-x-10'>
          <div className='form-group mb-6'>
            <label htmlFor='name' className='pb-4 px-1'>
              Nombre
            </label>
            <input
              id='name'
              className='input'
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </div>
          <div className='form-group mb-6'>
            <label htmlFor='lastname' className='pb-4 px-1'>
              Apellido
            </label>
            <input
              id='lastname'
              className='input'
              value={lastname}
              onChange={(e) => setLastname(e.target.value)}
            />
          </div>
        </div>

        <div className='form-group mb-6 '>
          <label htmlFor='street' className='pb-4 px-1'>
            Direccion
          </label>
          <input
            type='text'
            className='input'
            id='street'
            placeholder='Direccion'
            value={address}
            onChange={(e) => setAddress(e.target.value)}
          />
        </div>

        <div className='form-group mb-6 '>
          <label htmlFor='street2' className='pb-4 px-1'>
            Referencia
          </label>
          <input
            type='text'
            className='input'
            id='street2'
            value={reference}
            onChange={(e) => setReference(e.target.value)}
          />
        </div>

        <div className='form-group mb-6'>
          <label htmlFor='name' className='pb-4 px-1'>
            Ciudad
          </label>
          <select
            className='input'
            value={province}
            onChange={(e) => setProvince(e.target.value)}
          >
            <option>City</option>
          </select>
        </div>
      </div>
      {/* <div className='w-full mb-10'>
              <div className='lg:pr-8'>
                <h1 className=' lg:text-1xl font-light tracking-tight text-gray-900 sm:text-3xl mb-5'>
                  Metodo de envio
                </h1>
              </div>
              <div className='grid grid-cols-2 gap-x-10 mb-10'>
                <div
                  className={`h-40 w-full bg-white rounded-xl p-6 border-4 border-gray-200cursor-pointer  `}
                >
                  <div className='flex flex-col'>
                    <strong className='flex justify-between'>
                      Standar{' '}
                      {shippingMethod === 'Standar' && (
                        <Check className={'text-indigo-600'} />
                      )}
                    </strong>
                    <span className=' text-gray-400 opacity-80'>
                      2-3 dias laborales
                    </span>
                  </div>
                  <h3 className='font-bold mt-10'>$200</h3>
                </div>
                <div
                  className={`h-40 w-full bg-white rounded-xl p-6 border-4 ${
                    shippingMethod === 'Premiun'
                      ? 'border-indigo-600'
                      : 'border-gray-200'
                  } cursor-pointer  `}
                  onClick={() => setShippingMethod('Premiun')}
                >
                  <div className='flex flex-col'>
                    <strong className='flex justify-between'>
                      Premium{' '}
                      {shippingMethod === 'Premiun' && (
                        <Check className={'text-indigo-600'} />
                      )}
                    </strong>
                    <span className=' text-gray-400 opacity-80'>
                      1 dia laboral
                    </span>
                  </div>
                  <h3 className='font-bold mt-10'>$400</h3>
                </div>
              </div>
            </div> */}
    </div>
  )
}

export default ContactInfo
