import { useState, useEffect } from 'react'
import ReactDOM from 'react-dom'

function Modal({ show, onClose }) {
  const [isBrowser, setIsBrowser] = useState(false)

  useEffect(() => {
    setIsBrowser(true)
  }, [])

  const handleClose = (e) => {
    e.preventDefault()
    onClose()
  }

  const modalContent = show ? (
    <div className='modal_overlay'>
      <div className='modal'>
        <button onClick={handleClose}>Cerrar</button>
      </div>
    </div>
  ) : null

  if (isBrowser) {
    return ReactDOM.createPortal(
      modalContent,
      document.getElementById('modal-root')
    )
  } else {
    return null
  }
}

export default Modal
