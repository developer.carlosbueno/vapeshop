import ReactDOM from 'react-dom'
import { useState, useEffect } from 'react'
import OrderCard from '@components/OrderCard'
import { useUIState, useUIDispatch } from '@hooks/useContextHook'

function OrdersModal({ show, onClose }) {
  const [isBrowser, setIsBrowser] = useState(false)
  const { orders, user } = useUIState()
  const { getMyOrder } = useUIDispatch()

  useEffect(() => {
    setIsBrowser(true)
  }, [])

  const getOrder = (id, token) => {
    getMyOrder(id, token)
  }

  const handleClose = (e) => {
    e.preventDefault()
    onClose()
  }

  const modalContent = show ? (
    <div className='modal_overlay'>
      <div className='orders-modal'>
        {/* Header */}
        <div className='flex justify-between items-center border-b pb-2'>
          <h1 className='text-2xl font-medium tracking-wider'>Historial</h1>
          <button onClick={handleClose}>Cerrar</button>
        </div>
        {/* Items */}
        <div className='grid gap-2 py-6'>
          {orders?.map((o) => (
            <OrderCard
              key={o._id}
              getOrder={() => getOrder(o._id, user.token)}
            />
          ))}
        </div>
        {/* Progress */}
        <div></div>
      </div>
    </div>
  ) : null

  if (isBrowser) {
    return ReactDOM.createPortal(
      modalContent,
      document.getElementById('modal-root')
    )
  } else {
    return null
  }
}

export default OrdersModal
