import { useState, useEffect } from 'react'
import ReactDOM from 'react-dom'
import { useUIState } from '@hooks/useContextHook'

//Components
import OrderItems from './OrderItem'
import OrderProgress from './OrderProgress'

function OrderModal({ show, onClose }) {
  const [isBrowser, setIsBrowser] = useState(false)
  const { order } = useUIState()

  useEffect(() => {
    setIsBrowser(true)
  }, [])

  const handleClose = (e) => {
    e.preventDefault()
    onClose()
  }

  const modalContent = show ? (
    <div className='modal_overlay'>
      <div className='order-modal'>
        {/* Header */}
        <div className='flex justify-between items-center border-b pb-2'>
          <h1 className='text-2xl font-medium tracking-wider'>84523</h1>
          <button onClick={handleClose}>Cerrar</button>
        </div>

        {/* Items */}
        <div className='grid gap-2 py-6'>
          {order?.orderItems?.map((i) => (
            <OrderItems key={i._id} {...i} />
          ))}
          <div className='m-auto w-fit mt-2'>
            <ul>
              <li className='grid grid-cols-2 gap-16'>
                <p>Subtotal</p>
                <p>$2,400</p>
              </li>
              <li className='grid grid-cols-2 gap-16 my-1'>
                <p>Delivery</p>
                <p>$200</p>
              </li>
              <li className='grid grid-cols-2 gap-16'>
                <p className='font-bold'>Total</p>
                <p>$2,600</p>
              </li>
            </ul>
          </div>
        </div>

        {/* Progress */}
        <OrderProgress />
      </div>
    </div>
  ) : null

  if (isBrowser) {
    return ReactDOM.createPortal(
      modalContent,
      document.getElementById('modal-root')
    )
  } else {
    return null
  }
}

export default OrderModal
