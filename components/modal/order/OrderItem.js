import Image from 'next/image'

const OrderItems = ({ qty, product }) => {
  return (
    <div className='flex border p-2 rounded-lg'>
      <div className='w-fit'>
        <Image
          width={120}
          height={120}
          src='https://www.elementvape.com/media/catalog/product/cache/177ff88e405dbdaf9e02a16630acb6f3/v/o/voopoo_-_argus_pod_cartridge_-_accessories_-_packaging.png'
        />
      </div>
      <div className='w-full md:ml-4 flex flex-col justify-between md:py-4'>
        <div className='flex justify-between'>
          <h3 className='lg:text-lg'>{product.title}</h3>
          <h2 className='hidden md:block lg:text-xl text-gray-700 font-bold'>
            ${product.price}
          </h2>
        </div>
        <div className='flex justify-between'>
          <p className='text-gray-500'>Qty: {qty}</p>
          <h2 className='lg:text-xl md:hidden text-gray-700 font-bold'>
            {product.price}
          </h2>
        </div>
      </div>
    </div>
  )
}

export default OrderItems
