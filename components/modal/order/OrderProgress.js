// Icons
import { AiFillCheckCircle } from 'react-icons/ai'

function OrderProgress() {
  return (
    <div className='progresses flex justify-center pt-4 border-t'>
      <div className='steps'>
        <span>
          <AiFillCheckCircle size={30} />
        </span>
      </div>

      <span className='line'></span>

      <div className='steps'>
        <AiFillCheckCircle size={30} />
      </div>

      <span className='line'></span>

      <div className='steps'>
        <span className='font-weight-bold'>Enviado</span>
      </div>

      <span className='line'></span>

      <div className='steps'>
        <span className='font-weight-bold'>Listo</span>
      </div>
    </div>
  )
}

export default OrderProgress
