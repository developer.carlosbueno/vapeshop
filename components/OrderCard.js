import { useState } from 'react'
//Components
import OrderModal from './modal/order/OrderModal'

//Icons
import { AiOutlineCheckCircle } from 'react-icons/ai'

function OrderCard({ getOrder }) {
  const [showModal, setShowModal] = useState(false)

  const handleShowModal = () => {
    getOrder()
    setShowModal(true)
  }

  return (
    <>
      <div
        onClick={handleShowModal}
        className='bg-green-400 mt-2 text-green-800 shadow-sm rounded-full tracking-wider py-2 flex justify-between items-center text-sm px-2 hover:shadow-md'
      >
        <div className='flex items-center'>
          <AiOutlineCheckCircle size={30} />
          <p className='ml-2'>8754</p>
        </div>
        <p className='font-semibold text-green-800'>$1,200</p>
      </div>
      <OrderModal show={showModal} onClose={() => setShowModal(false)} />
    </>
  )
}

export default OrderCard
