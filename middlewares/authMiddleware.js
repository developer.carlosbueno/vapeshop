import jwt from 'jsonwebtoken'
import User from '@models/users'
import response from '@utils/response'
import { connect, disconnect } from '@utils/db'

export const protect = async (req, res, next) => {
  let token
  let decoded
  let authorization = req.headers.authorization
  try {
    if (authorization && authorization.startsWith('Bearer')) {
      token = authorization.split(' ')[1]
      decoded = await decodeToken(token)
      await connect()
      req.user = await User.findById(decoded.id).select('-password')
      await disconnect()
      next()
    } else if (!token) {
      response(res, 401, 'No authorization, no token')
    } else {
      response(res, 401, 'No authorize')
    }
  } catch (error) {
    response(res, 400, error.message)
  }
}

export const admin = (req, res, next) => {
  if (req.user && req.user.isAdmin) {
    next()
  } else {
    response(res, 401, 'Not admin token')
  }
}

const decodeToken = (token) => {
  return jwt.verify(token, process.env.JWT_SECRET)
}
